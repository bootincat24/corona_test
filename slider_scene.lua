
local composer = require( "composer" )
local file_handler = require("file_handler")  -- 'File_handler' module is for avoiding Android resource files limitation
local load_save = require( "load_save" )  -- 'Load_save' module is for converting JSON file to LUA table
local widget = require("widget")

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

--Initialize variables--
--Configure scroll view widget
local scroll_view
local params = {}
params.progress = true

-- Images configuration
local count = 0  -- Count of success downloaded images
local indent = 50
local def_image_height = display.contentHeight/3

-- Get an access and read the JSON file
file_handler.copyFile( "images.json.txt", nil, "images.json", system.DocumentsDirectory, true )
local images_table = load_save.loadTable("images.json", system.DocumentsDirectory).images

local function networkListener( event )
    if ( event.isError ) then
        print( "Network error - download failed: ", event.response )
    elseif ( event.phase == "began" ) then
        print( "Progress Phase: began" )
    elseif ( event.phase == "ended" ) then
        print( "Displaying response image file" )

        count = count + 1

        -- Configure downloaded image
        local image = display.newImageRect( event.response.filename,
                                            event.response.baseDirectory,
                                            display.contentWidth,
                                            def_image_height )
        image.alpha = 0
		    image.x = display.contentCenterX
		    image.y = (def_image_height/2)* (count - 1) + count*(indent + def_image_height/2)
        transition.to(image, {alpha=1.0})

		    scroll_view:insert(image)
    end
end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local scene_group = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen

  scroll_view = widget.newScrollView()
	scene_group:insert(scroll_view)

 -- if images table isn't nil
	if (images_table) then
        -- Download images by URL from JSON file
    	for i=1, #images_table, 1 do
    	   network.download(images_table[i],
                          "GET",
                          networkListener,
    	                    params,
    	                    "image"..i..".png",
    	                    system.TemporaryDirectory)
    	end
	end
end

-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
